var fileInput = document.getElementById('myfile');
var fReader = new FileReader();

fReader.onload = function(e) {
  var ind = {}
  arr = new Uint8Array(e.target.result);
  var asciiDec = new TextDecoder("ascii");
  var s = 0
  var e = 4
  ind['signature']=asciiDec.decode(arr.slice(s,e))
  ind['version']=intFromBytes(arr.slice(s=s+4,e=e+4))
  ind['entries']=intFromBytes(arr.slice(s=s+4,e=e+4))
    
  console.log(ind)
  
  for (n = 0; n < ind['entries']; n++) {
  	var entry = {}
    entry["entry"] = n + 1
    //entry["ctime_seconds"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["ctime_nanoseconds"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["mtime_seconds"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["mtime_nanoseconds"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["dev"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["ino"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["mode"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["uid"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["gid"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["size"]=intFromBytes(arr.slice(s=s+4,e=e+4))
    //entry["sha1"] =asciiDec.decode(arr.slice(s=s+4,e=e+20))
    //entry["flags"] =intFromBytes(arr.slice(s=s+20,e=e+2))
    len=intFromBytes(arr.slice(s=s+64,e=e+62))
    entry["name"] =asciiDec.decode(arr.slice(s=s+2,e=e+len))
    mod = e%8
    if ((8-mod) <= mod) { pad = 8-mod }
    else { pad = -mod }
   
    s=e+pad
    e=s+4
    console.log(entry)
	}
}

fileInput.onchange = function(e) {
    var file = this.files[0];
    fReader.readAsArrayBuffer(file);
    }

function intFromBytes( x ){
    var val = 0;
    for (var i = 0; i < x.length; ++i) {        
        val += x[i];        
        if (i < x.length-1) {
            val = val << 8;
        }
    }
    return val;
}
    